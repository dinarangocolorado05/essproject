import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { CatalogoComponent } from './componentes/catalogo/catalogo.component';
import { PedidosComponent } from './componentes/pedidos/pedidos.component';
import { ContactanosComponent } from './componentes/contactanos/contactanos.component';
import { PerfilComponent } from './componentes/perfil/perfil.component';
import { CarritosComponent } from './componentes/carritos/carritos.component';
import { InicioSesionComponent } from './componentes/inicio-sesion/inicio-sesion.component';
import { RegistroUComponent } from './componentes/registro-u/registro-u.component';

export const routes: Routes = [
    {path: '', component: InicioComponent}, 
    {path: 'catalogo', component: CatalogoComponent}, 
    {path: 'pedidos', component: PedidosComponent}, 
    {path: 'contactanos', component: ContactanosComponent},
    {path: 'inicio-sesion', component:InicioSesionComponent},
    {path: 'registro-u', component: RegistroUComponent},
    {path: 'perfil', component: PerfilComponent}, 
    {path: 'carrito', component: CarritosComponent}, 
   
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppModule { }

