import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { CatalogoComponent } from "./componentes/catalogo/catalogo.component";
import { HeaderComponent } from "./componentes/header/header.component";
import { PedidosComponent } from "./componentes/pedidos/pedidos.component";
import { FooterComponent } from "./componentes/footer/footer.component";
import { ContactanosComponent } from "./componentes/contactanos/contactanos.component";
import { InicioSesionComponent } from "./componentes/inicio-sesion/inicio-sesion.component";
import { RegistroUComponent } from "./componentes/registro-u/registro-u.component";
import { CarritosComponent } from "./componentes/carritos/carritos.component";
import { InicioComponent } from "./componentes/inicio/inicio.component";

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    imports: [CommonModule, RouterOutlet, CatalogoComponent, HeaderComponent, PedidosComponent, FooterComponent, ContactanosComponent, InicioSesionComponent, RegistroUComponent, CarritosComponent, InicioComponent]
})
export class AppComponent {
  title = 'client';
}
