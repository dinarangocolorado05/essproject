export class Productos {

    _id?: number; 
    producto: string; 
    cantidad: number; 
    cliente: string; 
    precio: number; 

    constructor(producto: string, cantidad: number, cliente: string, precio: number){
        this.producto = producto; 
        this.cantidad = cantidad; 
        this.cliente = cliente; 
        this.precio = precio; 
    }

}