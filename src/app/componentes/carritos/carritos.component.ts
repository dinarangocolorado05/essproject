import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-carritos',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './carritos.component.html',
  styleUrl: './carritos.component.css'
})
export class CarritosComponent {

}
