import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-registro-u',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './registro-u.component.html',
  styleUrl: './registro-u.component.css'
})
export class RegistroUComponent {

}
